solfege (3.23.4-13) unstable; urgency=medium

  * Build with python 3.13 (Closes: #1091432)

 -- Francois Mazen <mzf@debian.org>  Fri, 27 Dec 2024 18:26:09 +0100

solfege (3.23.4-12) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Archive.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Fix day-of-week for changelog entry 3.11.3-1.

  [ Francois Mazen ]
  * Set Gtk version to 3.0 explicitly.
  * Use execute_after_dh_link-indep target instead of override_dh_link
    to work around jdupes behavior change (Closes: #1050524).
  * Update standards version to 4.6.2, no changes needed.

 -- Francois Mazen <francois@mzf.fr>  Sun, 03 Sep 2023 15:54:35 +0200

solfege (3.23.4-11) unstable; urgency=medium

  * Source only upload for migration to testing

 -- Francois Mazen <francois@mzf.fr>  Tue, 20 Apr 2021 22:02:00 +0200

solfege (3.23.4-10) unstable; urgency=medium

  * Fix broken simlink (Closes: #986470)
  * Update Standards-Version to 4.5.1.

 -- Francois Mazen <francois@mzf.fr>  Mon, 05 Apr 2021 01:56:42 +0200

solfege (3.23.4-9) unstable; urgency=medium

  * Display message when documentation is not installed (Closes: #949294)
  * Replace unversioned python call to explicit python3 (Closes: #967212)
  * Move documentation in /usr/share/doc and register html files with doc-base
  * Remove duplicate files in documentation with jdupes

 -- Francois Mazen <francois@mzf.fr>  Mon, 03 Aug 2020 19:37:38 +0200

solfege (3.23.4-8) unstable; urgency=medium

  * Fix python3 warning (Closes: #955170).
  * Update VCS url.
  * Set debhelper-compat to 13.
  * Use yelp-tools to generate documentation.
  * Fix national encoding Lintian warning.
  * Forward patches to upstream.
  * Recommends solfege-doc for solfege (Bug#949294).

 -- Francois Mazen <francois@mzf.fr>  Sat, 09 May 2020 17:25:52 +0200

solfege (3.23.4-7) unstable; urgency=medium

  * Add dh-python dependency (Closes: #951890)
  * Remove gnome-doc-utils dependency (Closes: #947539)
  * Fix UTF-8 issue with tex2any and topdocs files.
  * Update Standards-Version to 4.5.0.
  * Update Debhelper compatibility to 12.

 -- Francois Mazen <francois@mzf.fr>  Sun, 23 Feb 2020 21:57:26 +0100

solfege (3.23.4-6) unstable; urgency=medium

  * Fix failing autopkgtest
  * Add Salsa VCS fields in control file
  * Setup Salsa CI

 -- Francois Mazen <francois@mzf.fr>  Mon, 04 Feb 2019 22:01:49 +0100

solfege (3.23.4-5) unstable; urgency=high

  * Fix webbrowser registering issue (Closes: #920639)
  * Update Standards Version to 4.3.0
  * Add simple autopkgtest

 -- Francois Mazen <francois@mzf.fr>  Mon, 28 Jan 2019 21:08:40 +0100

solfege (3.23.4-4) unstable; urgency=low

  * Fix mismatch between intermediate eps files when converting lilypond files
    to png files. This issue lead to reproducible issue.
  * Add Comment entry to desktop file to fix AppStream error.

 -- Francois Mazen <francois@mzf.fr>  Sun, 19 Aug 2018 12:01:06 +0200

solfege (3.23.4-3) unstable; urgency=low

  * New maintainer (Closes: #762451)
  * Updating Standards-Version to 4.2.0
  * Add sensible-utils dependency
  * Remove trailing whitespaces in changelog file
  * Add keywords to desktop file

 -- Francois Mazen <francois@mzf.fr>  Thu, 16 Aug 2018 14:54:29 +0200

solfege (3.23.4-2) unstable; urgency=low

  * QA upload.
  * Mark solfege-oss as Multi-Arch: same.
  * Add lintian override for dependency-is-not-multi-archified, because
    solfege can't be multi-archified.
  * Add patch to sort languages in generated file to allow reproducible build.

 -- Reiner Herrmann <reiner@reiner-h.de>  Fri, 16 Feb 2018 00:42:33 +0100

solfege (3.23.4-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
    - no longer requires rsvg (LP: #1004485)
  * Orphan package (see #762445).
  * debian/control:
    - update Homepage
    - update build dependencies for port to python3 / gtk3
    - drop recommendation of pyalsa, which is only available for python2
    - mark solfege-doc as Multi-Arch: foreign
    - minor improvements to descriptions
  * debian/patches:
    - refresh patches
    - add patch to fix warnings during autoreconf causing build failure
    - add patch to honour CPPFLAGS to support passing hardening flags
    - add patch to fix gtk deprecation warnings
    - add patch to workaround hidden menubar texts
    - re-add xvfb-run patch to series file
    - drop patch that disabled gettext test (running fine now)
    - drop patch that disabled pot rebuilding, as the target is not run
    - add DEP3 headers
  * Update Standards-Version to 4.1.3
    - drop menu file
  * Update debhelper compatibility to 11.
  * Add debian/clean to properly clean generated files.
  * Remove trailing whitespace in debian/changelog.
  * Build with all hardening options.
  * Point watch file to sf.net (the recent versions are currently
    distributed there).
  * Update paths of installed files in debian/*.install.
  * Enable running tests during build again
    - set UTF-8 locale, which is currently required by tests
  * Convert debian/copyright to copyright format 1.0 and add myself.
  * Run wrap-and-sort.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 10 Feb 2018 19:33:02 +0100

solfege (3.22.2-2) unstable; urgency=low

  * Don't run tests (closes: #726462)

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 22 Oct 2013 21:04:47 +0200

solfege (3.22.2-1) unstable; urgency=low

  * New upstream release
  * Don't build depend on any more xvfb

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 04 Oct 2013 18:41:34 +0200

solfege (3.20.6-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 21 Jun 2012 00:16:29 +0200

solfege (3.20.5-rc2-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 27 May 2012 22:52:03 +0200

solfege (3.20.3-4) unstable; urgency=low

  * python-support -> dh_python2
  * Recommends: python-pyalsa (closes: #644369)

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 15 Oct 2011 20:03:30 +0200

solfege (3.20.3-3) unstable; urgency=low

  * revert 3.20.3-2, it did not fix 643609.
  * don't run xmllint (closes: #643609)

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 04 Oct 2011 19:13:23 +0200

solfege (3.20.3-2) unstable; urgency=low

  * xmllint: run with --nonet (closes: #643609)

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 03 Oct 2011 22:31:44 +0200

solfege (3.20.3-1) unstable; urgency=low

  * New upstream release
  * solfege.desktop: add GenericName (closes: #641741)

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 25 Sep 2011 17:22:41 +0200

solfege (3.19.6-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 10 Apr 2011 11:04:33 +0200

solfege (3.16.4-2) unstable; urgency=low

  * Fix the test suite so that it builds (closes: #595860)

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 19 Sep 2010 16:40:13 +0200

solfege (3.16.4-1) unstable; urgency=low

  * New upstream release
  * Fix solfege-oss description (closes: #589656)
  * Do not raise Python string exceptions (closes: #585356)
  * Updated debian/watch file

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 24 Jul 2010 12:35:52 +0200

solfege (3.16.3-2) unstable; urgency=low

  * Section: extra, not optional

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 20 May 2010 21:47:24 +0200

solfege (3.16.3-1) unstable; urgency=low

  * New upstream release
  * Recommend csound (closes: #579210)

 -- Tom Cato Amundsen <tca@debian.org>  Wed, 19 May 2010 23:10:09 +0200

solfege (3.15.9-1) unstable; urgency=low

  * New upstream release.
  * Redo packaging.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 28 Mar 2010 06:34:28 +0200

solfege (3.14.5-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Ensure xmlllint does not access the net during build: new dpatch
    xmllint-nonet from Ubuntu by Alessio Treglia. (Closes: #533993)

 -- Stefano Zacchiroli <zack@debian.org>  Tue, 24 Nov 2009 10:01:25 +0100

solfege (3.14.5-2) unstable; urgency=low

  * Don't depent on python-gtkhtml2 (closes: #549032)

 -- Tom Cato Amundsen <tca@debian.org>  Wed, 23 Sep 2009 04:19:19 +0200

solfege (3.14.5-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 05 Jul 2009 00:27:23 +0200

solfege (3.11.3-4) unstable; urgency=low

  * added patches/xsltproc-nonet.dpatch to run xsltproc without net
    access. (Closes: #494228)

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 15 Aug 2008 09:40:22 +0200

solfege (3.11.3-3) unstable; urgency=low

  * don't code on vacation: add patches/filesystem-getcwd-bug to
    patches/00list

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 09 Aug 2008 10:22:11 +0200

solfege (3.11.3-2) unstable; urgency=low

  * added patches/filesystem-getcwd-bug. One line fix to replace
    getcwd with os.getcwdu. (Closes: #492818)

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 31 Jul 2008 23:01:17 +0200

solfege (3.11.3-1) unstable; urgency=low

  * New upstream release
  * Adjusted pygtk version requirement.
  * Modified debian/rules to use the dh script
  * Build-depend on libxml2-utils since the tests use xmllint
  * Build-depend on gnome-doc-utils

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 14 Jul 2008 07:46:47 +0200

solfege (3.10.3-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 25 Mar 2008 23:56:07 +0100

solfege (3.10.1-1) unstable; urgency=low

  * New upstream release.
  * Remove the Bzr-Vcs field again. It should point the a repo of the
    debian dir, and not the upstream repo.

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 07 Feb 2008 22:16:59 +0100

solfege (3.9.3-4) unstable; urgency=low

  * Added Bzr-Vcs field to the control file.

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 27 Nov 2007 22:14:44 +0100

solfege (3.9.3-3) unstable; urgency=low

  * Build depend on xbase-clients and xfonts-base too. xvfb need this.
    (Closes: #449146, #449196)

 -- Tom Cato Amundsen <tca@debian.org>  Sun,  4 Nov 2007 14:05:18 +0100

solfege (3.9.3-2) unstable; urgency=low

  * Build depend on xvfb. (Closes: #449146)
  * Added Vcs-Bzr field.

 -- Tom Cato Amundsen <tca@debian.org>  Sat,  3 Nov 2007 18:15:44 +0100

solfege (3.9.3-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Fri,  2 Nov 2007 11:50:00 +0100

solfege (3.8.1-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 18 Jun 2007 22:11:22 +0200

solfege (3.6.4-1) unstable; urgency=medium

  * New upstream release that consists of only small (in number of
    changed lines) fixes we to go into etch.
  * Handle upgrade from 3.0.6-1 (closes: #399214)

 -- Tom Cato Amundsen <tca@debian.org>  Fri,  8 Dec 2006 20:39:34 +0100

solfege (3.6.3-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 17 Oct 2006 20:54:08 +0200

solfege (3.6.2-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 16 Oct 2006 10:03:14 +0200

solfege (3.6.0-1) unstable; urgency=low

  * New upstream release.
  * Cleanup after old release missing solfege.dirs. (closes: #389523)
  * Serious attempt to follow the new python policy. (closes: 374757)
  * Depend on timidity and freepats, because this is the easiest and
    safest way to provide working sound out of the box.

 -- Tom Cato Amundsen <tca@debian.org>  Mon,  2 Oct 2006 22:30:39 +0200

solfege (3.4.1-1) unstable; urgency=low

  * New upstream release (#closes: 380578)

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 31 Jul 2006 06:33:06 +0200

solfege (3.4.0-4) unstable; urgency=low

  * Remove "exit -1" bashism from two makefiles. (closes: #377363)

 -- Tom Cato Amundsen <tca@debian.org>  Sat,  8 Jul 2006 21:30:27 +0200

solfege (3.4.0-3) unstable; urgency=low

  * Build with skipmanual=yes.

 -- Tom Cato Amundsen <tca@debian.org>  Wed,  5 Jul 2006 23:10:28 +0200

solfege (3.4.0-2) unstable; urgency=low

  * Added XS-Python-Version: current
  * Added XB-Python-Version: ${python:Versions}

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 29 Jun 2006 22:23:49 +0200

solfege (3.4.0-1) unstable; urgency=low

  * New upstream release.
  * Use dh_pysupport
  * Use dh_installman instead of dh_installmanpages
  * Rename /etc/solfege2.0 to /etc/solfege in preinst if it exists.
  * Adjusted debian/solfege.doc-base to find the new user manual
  * Join the old and the new changelog file into
    /usr/share/solfege/changelog.gz, not only the old.

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 29 Jun 2006 20:44:21 +0200

solfege (3.0.3-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 14 Oct 2005 23:13:05 +0200

solfege (3.0.2-1) unstable; urgency=low

  * New upstream release.
  * soundcard/Makefile fix (closes: #331218)
  * german PO file update (closes: #313904)

 -- Tom Cato Amundsen <tca@debian.org>  Mon,  3 Oct 2005 13:52:10 +0200

solfege (3.0.1-1) unstable; urgency=low

  * New upstream release. (closes: #300447)
  * Depend on python-gnome2-extras (closes: #330478)

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 30 Sep 2005 18:20:19 +0200

solfege (2.1.9-1) experimental; urgency=low

  * New upstream release.
  * Don't call update_gui_after_lessonfile_change for exercises
    that are not lesson file based (closes: #283285)

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 10 Dec 2004 22:09:24 +0100

solfege (2.1.8-1) experimental; urgency=low

  * Test package of the latest devel release
  * Handle the rename of /etc/solfegeX.Y => /etc/solfege
  * Added build-depends on lilypond, gs-gpl and librsvg2-bin

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 25 Oct 2004 23:28:36 +0200

solfege (2.1.7-1) unstable; urgency=low

  * Test package of the latest devel release

 -- Tom Cato Amundsen <tca@debian.org>  Mon,  4 Oct 2004 23:52:47 +0200

solfege (2.0.4-2) unstable; urgency=low

  * Closes: #225332: missing dependancy on python2.2
  * Build gpl-appendix.html and fdl-appendix.html without net access.

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 29 Dec 2003 17:15:04 +0100

solfege (2.0.4-1) unstable; urgency=low

  * New upstream release. This release silently replaces characters
  with bad encoding. (closes: #222428)

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 27 Dec 2003 17:35:04 +0100

solfege (2.0.1-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Tue,  9 Sep 2003 16:01:07 +0200

solfege (2.0.0-1) unstable; urgency=low

  * New upstream release.
  * Added build-depends on python-gtk2-dev

 -- Tom Cato Amundsen <tca@debian.org>  Sun,  1 Sept 2003 10:02:43 +0200

solfege (1.9.11-1) unstable; urgency=low

  * New upstream release.
  * Fix broken Build-Depends. (closes: #201228)
  * Change menu placement to Apps/Education.
  * Standards-Version: 3.6.0

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 17 Jul 2003 22:25:09 +0200

solfege (1.9.99-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Fri,  1 Aug 2003 00:53:17 +0200

solfege (1.9.9-2) unstable; urgency=low

  * Change to Section: gnome

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 24 Mar 2003 22:19:40 +0100

solfege (1.9.9-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 24 Mar 2003 22:19:40 +0100

solfege (1.9.8-1) unstable; urgency=low

  * New devel release.

 -- Tom Cato Amundsen <tca@debian.org>  Mon,  3 Feb 2003 14:10:22 +0100

solfege (1.9.7-3) unstable; urgency=low

  * Build depend on docbook-xml since we try to avoid getting the DTD
    from the net.

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 10 Jan 2003 16:28:22 +0100

solfege (1.9.7-2) unstable; urgency=low

  * online-docs/es_MX/solfege.py: don't refer to the dtd using
    file:///path/to/file, use http://www.oasis-open.org etc...
  * configure using --with-local-xmlcatalog.

 -- Tom Cato Amundsen <tca@debian.org>  Thu,  9 Jan 2003 23:12:30 +0100

solfege (1.9.7-1) unstable; urgency=low

  * New devel release.

 -- Tom Cato Amundsen <tca@debian.org>  Wed,  8 Jan 2003 22:48:54 +0100

solfege (1.9.6-1) unstable; urgency=low

  * New devel release.

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 28 Dec 2002 18:41:40 +0100

solfege (1.9.5-3) unstable; urgency=low

  * Online docs: get docbook dtd from the net.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 22 Dec 2002 22:32:52 +0100

solfege (1.9.5-2) unstable; urgency=low

  * New libgtkhtml-2 test.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 22 Dec 2002 19:39:40 +0100

solfege (1.9.5-1) unstable; urgency=low

  * New devel release. First 1.9.x upload to debian.

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 20 Dec 2002 18:55:44 +0100

solfege (1.9.2-2) unstable; urgency=low

  * New devel release.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 18 Aug 2002 22:18:18 +0200

solfege (1.5.0-1) unstable; urgency=low

  * New upstream development release.

 -- Tom Cato Amundsen <tca@debian.org>  Thu,  7 Feb 2002 23:54:53 +0100

solfege (1.4.0-1) unstable; urgency=low

  * New upstream release.
  * Fix spelling error in package description. (closes: #125371)

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 22 Dec 2001 18:58:45 +0100

solfege (1.2.2-1) unstable; urgency=low
  * Build depend on new libgtkhtml20 (closes: #124023). Use gtkhtml
  instead of xmhtml as html widget.
  * New stable release.
  * Added solfege.doc-base

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 14 Dec 2001 23:24:53 +0100

solfege (1.2.1-2) unstable; urgency=low

  * Updated to new python policy. (closes: #118522)
  * make /etc/gnome/config/solfege1.2 a conffile
  * Added build-depends on python since the configure script use the
  python interpreter.

 -- Tom Cato Amundsen <tca@debian.org>  Wed,  7 Nov 2001 19:01:16 +0100

solfege (1.2.1-1) unstable; urgency=low

  * New stable release.

 -- Tom Cato Amundsen <tca@debian.org>  Sun,  9 Sep 2001 09:37:18 +0200

solfege (1.1.5-1) unstable; urgency=low

  * New development release. This is release candidate 2, and a
  new stable release will be out soon.
  * Added support for tuplets. (closes: #101261)
  * Added some build-depends.
  * Depends and Build-Depends on python 1.5.2. Solfege works with
  python 2.x, but we need a GPL compatible python.

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 11 Aug 2001 00:20:17 +0200

solfege (1.0.4-1) unstable; urgency=low

  * Fixes screwed up debian/rules. (closes: #96785) Since I am both
  debian developer and upstream I give it a new upstream version
  and does this upload the proper way with a diff.gz so I can fix
  packaging errors without uploading a new tarball.

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 10 May 2001 00:16:16 +0200

solfege (1.0.3-1) unstable; urgency=low

  * New upstream release. Bugfixes to sound init code and added
  dutch translations. Bugfix to random_transpose.

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 29 Apr 2001 13:44:10 +0200

solfege (1.0.1-1) unstable; urgency=medium

  * New upstream release. Bugfixes only.
  * This release will try to treat m68k as the other arches to see
  if it will build with OSS sound support.

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 12 Apr 2001 21:15:03 +0200

solfege (1.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Sun,  1 Apr 2001 15:25:26 +0200

solfege (0.7.38-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Thu, 15 Mar 2001 23:15:08 +0100

solfege (0.7.35-1) unstable; urgency=low

  * New upstream release.
  * Debian package for m68k is configure with --disable-oss-sound
  until the binutils bugs are fixed.

 -- Tom Cato Amundsen <tca@debian.org>  Mon,  5 Mar 2001 18:02:48 +0100

solfege (0.7.33-1) unstable; urgency=medium

  * New upstream release. Bugfixes.

 -- Tom Cato Amundsen <tca@debian.org>  Wed, 28 Feb 2001 12:25:08 +0100

solfege (0.7.32-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Mon, 26 Feb 2001 00:26:51 +0100

solfege (0.7.31-1) unstable; urgency=low

  * New upstream release. Mostly bugfixes.
  * Standards-Version: 3.5.1.0

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 23 Feb 2001 19:21:58 +0100

solfege (0.7.30-1) unstable; urgency=low

  * New upstream release.

 -- Tom Cato Amundsen <tca@debian.org>  Tue, 20 Feb 2001 22:41:53 +0100

solfege (0.7.29-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Fri,  2 Feb 2001 22:51:24 +0100

solfege (0.7.28-2) unstable; urgency=low

  * Added python-gnome to build-depends (closes: #84367)

 -- Tom Cato Amundsen <tca@debian.org>  Thu,  1 Feb 2001 21:04:53 +0100

solfege (0.7.28-1) unstable; urgency=low

  * New upstream release

 -- Tom Cato Amundsen <tca@debian.org>  Sun, 21 Jan 2001 22:21:29 +0100

solfege (0.7.27-1) unstable; urgency=low

  * New upsteam release

 -- Tom Cato Amundsen <tca@debian.org>  Sat,  9 Dec 2000 18:05:42 +0100

solfege (0.7.26-2) unstable; urgency=low

  * Don't import gtkhtml unless the user want to use it (closes: #77306)

 -- Tom Cato Amundsen <tca@debian.org>  Sat, 18 Nov 2000 02:49:48 +0100

solfege (0.7.26-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@debian.org>  Fri, 10 Nov 2000 22:39:35 +0100

solfege (0.7.25-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@debian.org>  Wed,  8 Nov 2000 22:14:32 +0100

solfege (0.7.24-1) unstable; urgency=low

  * New development release, only minor fixes.

 -- Tom Cato Amundsen <tca@gnu.org>  Sat, 14 Oct 2000 00:33:58 +0200

solfege (0.7.23-1) unstable; urgency=low

  * New development release, only minor fixes.

 -- Tom Cato Amundsen <tca@gnu.org>  Thu, 28 Sep 2000 21:51:20 +0200

solfege (0.7.22-1) unstable; urgency=low

  * New development release, fixing problem with music displayer
  when using gtk-themes.

 -- Tom Cato Amundsen <tca@gnu.org>  Wed, 27 Sep 2000 23:59:01 +0200

solfege (0.7.21-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@gnu.org>  Sun, 10 Sep 2000 16:45:17 +0200

solfege (0.7.20-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@gnu.org>  Fri, 25 Aug 2000 01:44:05 +0200

solfege (0.7.19-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@gnu.org>  Mon, 21 Aug 2000 21:30:16 +0200

solfege (0.7.18-1) unstable; urgency=low

  * Small update to 0.7.17

 -- Tom Cato Amundsen <tca@gnu.org>  Sat,  5 Aug 2000 15:12:17 +0200

solfege (0.7.17-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tca@gnu.org>  Fri,  4 Aug 2000 17:33:55 +0200

solfege (0.7.16-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Tue, 25 Jul 2000 00:11:58 +0200

solfege (0.7.15-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sat,  1 Jul 2000 15:48:17 +0200

solfege (0.7.10-2.1) unstable; urgency=low

  * NMU with consent by maintainer Tom Cato Amundsen because his sponser is
    currently abroad.
  * Added build dependency on python-dev (closes: #65870).

 -- Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>  Tue, 27 Jun 2000 11:43:12 +0200

solfege (0.7.14-1) unstable; urgency=low

  * New development release
  * Added Build-Depends: python-dev. This was included in a source NMU
  to 0.7.10 to close #65870

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 26 Jun 2000 22:58:48 +0200

solfege (0.7.13-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sun,  4 Jun 2000 17:24:09 +0200

solfege (0.7.12-1) unstable; urgency=low

  * Sorry, 0.7.11 was useless

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 29 May 2000 23:44:14 +0200

solfege (0.7.11-1) unstable; urgency=low

  * New development release
  * No longer Depend on debconf since it is not used any more

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 29 May 2000 22:39:35 +0200

solfege (0.7.10-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Thu,  4 May 2000 22:12:22 +0200

solfege (0.7.9-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sat, 29 Apr 2000 12:14:38 +0200

solfege (0.7.8-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sun,  9 Apr 2000 21:15:12 +0200

solfege (0.7.7-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Fri,  7 Apr 2000 15:19:01 +0200

solfege (0.7.6-1) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sun, 26 Mar 2000 20:40:51 +0200

solfege (0.7.5-1) unstable; urgency=low

  * New development release.

 -- Tom Cato Amundsen <tomcato@iname.com>  Fri, 17 Mar 2000 00:36:33 +0100

solfege (0.7.4) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sun,  5 Mar 2000 23:34:50 +0100

solfege (0.7.3-1) unstable; urgency=low

  * Fix broken upload of 0.7.3

 -- Tom Cato Amundsen <tomcato@iname.com>  Wed, 23 Feb 2000 23:04:36 +0100

solfege (0.7.3) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Wed, 23 Feb 2000 23:03:37 +0100

solfege (0.7.2) unstable; urgency=low

  * New development release

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 14 Feb 2000 12:14:39 +0100

solfege (0.7.1pre2) unstable; urgency=low

  * Testrelease

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 14 Feb 2000 12:11:43 +0100

solfege (0.5.6-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 24 Jan 2000 00:48:42 +0100

solfege (0.5.5-2) unstable; urgency=low

  * Same as 0.5.5-1, need a new filename to please the server.

 -- Tom Cato Amundsen <tomcato@iname.com>  Fri, 21 Jan 2000 20:20:12 +0100

solfege (0.5.5-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Fri, 21 Jan 2000 18:50:53 +0100

solfege (0.5.4-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Tue, 18 Jan 2000 08:15:35 +0100

solfege (0.5.3-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 17 Jan 2000 20:39:28 +0100

solfege (0.5.2-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Sun, 16 Jan 2000 16:27:31 +0100

solfege (0.5.1-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Fri, 14 Jan 2000 16:55:44 +0100

solfege (0.5-1) unstable; urgency=low

  * New release

 -- Tom Cato Amundsen <tomcato@iname.com>  Wed, 12 Jan 2000 00:02:08 +0100

solfege (0.4.2-1) unstable; urgency=low

  * Initial release of debian package. not yet part of the debian distro.

 -- Tom Cato Amundsen <tomcato@iname.com>  Mon, 10 Jan 2000 15:33:46 +0100

Local variables:
mode: debian-changelog
End:

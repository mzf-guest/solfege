Document: solfege-doc-es
Title: GNU Solfege Help (Spanish)
Author: Tom Cato Amundsen
Abstract: Manual of the GNU solfege software.
Section: Education

Format: HTML
Index: /usr/share/doc/solfege/help/es/index.html
Files: /usr/share/doc/solfege/help/es/*.html
